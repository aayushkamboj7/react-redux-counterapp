import { connect } from 'react-redux';
import './App.css';

function App(props) {
  function handleClick(actionType) {
    props.dispatch({ type: actionType });
  }

  return (
    <div>
      <h1>{props.count}</h1>
      <button onClick={(event) => handleClick(event.target.innerText)}>
        Increment
      </button>
      <button onClick={(event) => handleClick(event.target.innerText)}>
        Decrement
      </button>
      <button onClick={(event) => handleClick(event.target.innerText)}>
        Reset
      </button>
    </div>
  );
}

function stateToProps(state) {
  return {
    count: state.value,
  };
}

export default connect(stateToProps)(App);
