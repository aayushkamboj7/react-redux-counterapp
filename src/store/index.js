import { createStore } from 'redux';

function counterReducer(state = { value: 0 }, action) {
  switch (action.type) {
    case 'Increment':
      return { value: state.value + 1 };
    case 'Decrement':
      return { value: state.value - 1 };
    case 'Reset':
      return { value: 0 };
    default:
      return state;
  }
}

let store = createStore(counterReducer);

export default store;
